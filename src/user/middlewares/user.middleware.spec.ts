import { UserMiddleware } from './user.middleware';
import { ConfigService } from '../../config';
import { AuthService } from '../services';
import { TokenPayloadModel } from '../models';

describe('UserMiddleware', () => {

  let middleware: UserMiddleware;

  const payload: TokenPayloadModel = {
    user: {id: 1, name: 'Piotr'},
  };
  const config: Partial<ConfigService> = {
    TOKEN_HEADER_NAME: 'api_token',
  };
  const token = 'test-token-string';

  const authService: Partial<AuthService> = {
    async tokenVerify(t: string): Promise<TokenPayloadModel> {
      return (t === token) ? payload : null;
    },
  };

  beforeAll(() => {
    middleware = new UserMiddleware(config as ConfigService, authService as AuthService);
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });

  it('should add tokenPayload to req', async () => {
    const req = {
      headers: {
        [config.TOKEN_HEADER_NAME]: token,
      },
      tokenPayload: undefined,
    };
    const next = jest.fn();

    await middleware.use(req, {}, next);

    expect(req.tokenPayload).toMatchObject(payload);

    expect(next).toHaveBeenCalled();
    
  });

  it('should not decode token', async () => {
    const req = {
      headers: {
        [config.TOKEN_HEADER_NAME]: 'fake token string',
      },
      tokenPayload: undefined,
    };
    const next = jest.fn();

    await middleware.use(req, {}, next);

    expect(req.tokenPayload).toBeUndefined();

    expect(next.mock.calls.length).toBe(1);
    
  });
});
