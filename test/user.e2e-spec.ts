import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { UserRegisterRequestDto, UserRegisterResponseDto, UserLoginRequestDto, UserLoginResponseDto } from '../src/user/dto';
import { TokenPayloadModel } from '../src/user/models';
import { ConfigService } from '../src/config';
import { AuthService } from '../src/user/services';

describe('UserController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/user/register (POST)', () => {
    const req: UserRegisterRequestDto = {
      name: 'piotr',
      email: 'piotr@myflow.pl',
      password: '123',
    };
    const res: UserRegisterResponseDto = {
      user: {
        id: expect.any(Number),
        name: 'piotr',
        email: 'piotr@myflow.pl',
      },
    };
    return request(app.getHttpServer())
      .post('/user/register')
      .send(req)
      .expect(201)
      .then(r => {
        expect(r.body).toMatchObject(res);
      });
  });

  it('/user/login SUCCESS', () => {
    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '123',
    };
    const resBody: UserLoginResponseDto = {
      token: expect.any(String),
      user: {
        id: expect.any(Number),
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      },
    };
    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(201)
      .then(res => {
        expect(res.body).toMatchObject(resBody);
      });
  });

  it('/user/login ERROR', () => {

    const req: UserLoginRequestDto = {
      email: 'piotr@myflow.pl',
      password: '0004',
    };

    return request(app.getHttpServer())
      .post('/user/login')
      .send(req)
      .expect(422);
  });

  it('/user (GET)', async () => {
    const resBody: TokenPayloadModel = {
      user: {
        id: 1,
        name: 'Piotr',
        email: 'piotr@myflow.pl',
      },
    };
    const config: ConfigService = app.get(ConfigService);
    const authService: AuthService = app.get(AuthService);
    const token = await authService.tokenSign(resBody);

    return request(app.getHttpServer())
      .get('/user')
      .set(config.TOKEN_HEADER_NAME, token)
      .expect(200)
      .then(res => {
        expect(res.body).toMatchObject(resBody.user);
      });
  });

  it('/user (GET) AuthGuard Forbidden', () => {
    return request(app.getHttpServer())
      .get('/user')
      .expect(403);
  });
});
