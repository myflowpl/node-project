import { SubscribeMessage, WebSocketGateway, OnGatewayInit, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { Logger } from '@nestjs/common';
import { BehaviorSubject } from 'rxjs';
import { throttleTime } from 'rxjs/operators';

@WebSocketGateway({ namespace: '/chat' })
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

  private users$ = new BehaviorSubject({});

  handleConnection(client: any, ...args: any[]) {
    const users = this.users$.getValue();

    users[client.id] = {
      client,
      name: '',
    };
    this.users$.next(users);
  }

  handleDisconnect(client: any) {
    const users = this.users$.getValue();
    delete users[client.id];
    this.users$.next(users);
  }

  @WebSocketServer() wss: Server;

  private logger: Logger = new Logger('ChatGateway');

  afterInit(server: any) {
    this.logger.log('Initialized!');

    this.users$.pipe(
      throttleTime(200),
    ).subscribe(users => {
      this.wss.emit(
        'users',
        Object.values(users).map((v: any) => v.name).filter(u => !!u));
    });
  }

  @SubscribeMessage('chatToServer')
  handleMessage(client: Socket, message: { sender: string, message: string }) {
    this.wss.emit('chatToClient', message);
  }

  @SubscribeMessage('login')
  handleLogin(client: Socket, message: { sender: string, message: string }) {
    const users = this.users$.getValue();
    users[client.id].name = message.sender;
    this.users$.next(users);
  }

}
