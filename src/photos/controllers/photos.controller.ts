import { Controller, Post, UseInterceptors, Body, UploadedFile, Get } from '@nestjs/common';
import { ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { PhotosService } from '../services/photos.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('photos')
export class PhotosController {

  constructor(private photosService: PhotosService) {}

  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Upload user avatar' })
  @Post('upload-user-avatar')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file, @Body() body) {
    const avatar = await this.photosService.create(file);
    const thumb = await this.photosService.createThumbs(avatar.photo.filename);
    return {avatar, thumb, file, body};
  }

  @Get()
  getPhotos() {
    return this.photosService.findAll();
  }
}
