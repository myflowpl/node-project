import { Module } from '@nestjs/common';
import { CommentsController } from './controllers/comments.controller';

@Module({
  imports: [],
  controllers: [CommentsController],
  providers: [],
  exports: [],
})
export class CommentsModule {}
