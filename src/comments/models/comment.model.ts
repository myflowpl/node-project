import { ApiModelProperty, ApiModelPropertyOptional } from "@nestjs/swagger";

export class CommentModel {
  @ApiModelPropertyOptional()
  id?: number;

  @ApiModelProperty({
    example: 'Piotr Example name',
    description: 'Name in string format'
  })
  name: string;
}
