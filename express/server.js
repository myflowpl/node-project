const express = require('express');
const app = express();


function loggerMiddleware(req, res, next) { // LOGGER
  console.log('LOGGER', req.url);
  next();
}

app.get('/express', 

  loggerMiddleware,

  function(req, res, next) { // AUTH
    console.log('AUTH', req.query.name);
    setTimeout(() => {
      req.user = req.query.name;
      next();
    }, 1000)
  },

  function(req, res, next) { // GUARD
    console.log('GUARD', req.url);
    req.user ? next() : next('Forbidden Error');
  },

  function(err, req, res, next) {
    console.log('ERROR 2', err);
    res.status(403);
    next('forbidden status');
  },

  function(req, res) {
    // ASYNC DB WORK

    // res.send('Hello world from Express.js')
    res.json({
      user: req.user,
      message: 'Hello world from Express.js 2'
    })
  },

  function(err, req, res, next) {
    console.log('ERROR 2', err);
    res.json({
      error: err
    })
  }
  

)

app.get('/users/:userId/books/:bookId', function (req, res) {
  res.send(req.params)
})

module.exports = app;

// app.listen(3000, () => console.log('WORKING...'))