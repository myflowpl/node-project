
export class GetContactRequestDto {
  search: string;
  pageIndex: number;
  pageSize: number;
}
