import { UserModel } from '../models';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsEmail } from 'class-validator';

export class UserRegisterRequestDto {
  @ApiModelProperty({example: 'Piotr'})
  name: string;
  @ApiModelProperty()
  email: string;
  @ApiModelProperty()
  password: string;
}

export class UserRegisterResponseDto {
  @ApiModelProperty()
  user: UserModel;
}

export class UserLoginRequestDto {
  
  @ApiModelProperty({example: 'piotr@myflow.pl'})
  @IsEmail()
  email: string;

  @ApiModelProperty({example: '123'})
  @IsString()
  @MinLength(3)
  password: string;
}

export class UserLoginResponseDto {
  @ApiModelProperty()
  token: string;
  @ApiModelProperty()
  user: UserModel;
}
