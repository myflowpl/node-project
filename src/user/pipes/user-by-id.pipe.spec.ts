import { UserByIdPipe } from './user-by-id.pipe';
import { UserService } from '../services';
import { UserModel } from '../models';
import { NotFoundException, BadRequestException } from '@nestjs/common';

describe('UserByIdPipe', () => {

  let pipe: UserByIdPipe;

  beforeEach(() => {

    const userService: Partial<UserService> = {
      async getById(id: number): Promise<UserModel> {
        return {
          id,
          name: 'Piotr',
        };
      },
    };

    pipe = new UserByIdPipe(userService as UserService);
  });

  it('should be defined', () => {
    expect(pipe).toBeDefined();
  });

  it('should map user id to UserModel', async () => {

    const user = await pipe.transform(3, undefined);

    expect(user).toMatchObject({
      id: 3,
      name: expect.any(String),
    });

  });

  it('should Throw Validation Error', () => {

    return expect(pipe.transform('dfsdf', undefined)).rejects.toThrowError(BadRequestException);

  });

});
