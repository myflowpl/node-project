import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRole } from '../models';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private readonly reflector: Reflector) { }
  
  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {

    const request = this.getRequest(context);
    
    if (!request.tokenPayload) {
      return false;
    }
    const roles = this.reflector.get<UserRole[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }

    const user = request.tokenPayload.user;
    const hasRole = () => !!user.roles.find(role => !!roles.find(item => item === role));
    return user && user.roles && hasRole();

  }

  getRequest(context) {
    return context.switchToHttp().getRequest();
  }
}
