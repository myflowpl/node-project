# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 0.1.0 (2019-06-07)


### Bug Fixes

* **upload:** usunieto cropa ([e461bfe](https://bitbucket.org/myflowpl/node-project/commit/e461bfe))


### Features

* **build:** shelljs dodany ([4dd48e5](https://bitbucket.org/myflowpl/node-project/commit/4dd48e5))
* **cli:** custom cli ([f487298](https://bitbucket.org/myflowpl/node-project/commit/f487298))
* **db:** dodano połączenie z sqllite ([d0211b2](https://bitbucket.org/myflowpl/node-project/commit/d0211b2))
* **microservices:** dodano mikroserwisy ([c2b5654](https://bitbucket.org/myflowpl/node-project/commit/c2b5654))
* **photos:** dodano tworzenie miniaturek ([153e2e6](https://bitbucket.org/myflowpl/node-project/commit/153e2e6))
* **photos:** dodano zapis zdjec do bazy danych ([c22a51a](https://bitbucket.org/myflowpl/node-project/commit/c22a51a))
* **websockets:** init web socket example ([bf83c3e](https://bitbucket.org/myflowpl/node-project/commit/bf83c3e))
