import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import * as express from '../express/server.js';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService, ConfigModule } from './config';
import { UserMiddleware } from './user/middlewares/user.middleware';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(express));
  app.useStaticAssets(join(__dirname, '..', 'assets'));
  app.enableShutdownHooks();
  app.setGlobalPrefix('api');

  const config: ConfigService = app.get(ConfigService);
  // const userMid: UserMiddleware = app.get(UserMiddleware);
  // app.use(userMid);
  
  const options = new DocumentBuilder()
    .setTitle('Nest API Example')
    .setDescription('Przykładowy projekt w Node.js i TypeScript')
    .setVersion('1.0')
    .setBasePath('api')
    .addTag('user')
    .addBearerAuth(config.TOKEN_HEADER_NAME, 'header')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api/docs', app, document);

  await app.listen(3000);
}
bootstrap();
