import { Module, Global } from '@nestjs/common';
import { ConfigService } from './config.service';

@Global()
@Module({
  providers: [{
    provide: ConfigService,
    async useFactory(db) {
      // const data = await db.getConfigFromDatabase();
      // return new ConfigService(data);
      return new ConfigService();
    }
  }],
  exports: [ConfigService],
})
export class ConfigModule {}
