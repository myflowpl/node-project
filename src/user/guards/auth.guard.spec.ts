import { AuthGuard } from './auth.guard';
import { Reflector } from '@nestjs/core';
import { ExecutionContext } from '@nestjs/common';
import { UserRole } from '../models';

describe('AuthGuard', () => {
  it('should be defined', () => {

    const reflector: any = {
      get() {
        return [UserRole.ADMIN];
      },
    };

    const context = {
      getHandler() {},
      switchToHttp() {
        return {
          getRequest() {
            return {
              tokenPayload: {
                user: {
                  id: 1,
                  name: 'Piotr',
                  roles: [UserRole.ADMIN],
                },
              },
            };
          },
        };
      },
    } as ExecutionContext;

    const guard = new AuthGuard(reflector);

    return expect(guard.canActivate(context)).resolves.toBeTruthy();

  });

});
