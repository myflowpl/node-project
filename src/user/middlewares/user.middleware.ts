import { Injectable, NestMiddleware } from '@nestjs/common';
import { UserRole } from '../models';
import { ConfigService } from '../../config';
import { AuthService } from '../services';

@Injectable()
export class UserMiddleware implements NestMiddleware {

  constructor(
    private config: ConfigService,
    private authService: AuthService
  ) { }

  async use(req, res, next) {
    if (req.headers[this.config.TOKEN_HEADER_NAME]) {
      const payload = await this.authService.tokenVerify(req.headers[this.config.TOKEN_HEADER_NAME]);
      if (payload) {
        console.log('payload', payload)
        req.tokenPayload = payload;
      }
    }
    next();
  }
}
