import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class WorkerController {

  @MessagePattern({ cmd: 'sum' })
  accumulate(data: number[]): number {
    console.log('DATA', data);
    return (data || []).reduce((a, b) => a + b);
  }

  @MessagePattern({ cmd: 'resize' })
  resize(data: number[]): number {
    console.log('DATA', data);
    return (data || []).reduce((a, b) => a + b);
  }
}
