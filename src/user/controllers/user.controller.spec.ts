import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { ConfigService } from '../../config';
import { AuthService, UserService } from '../services';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';

describe('User Controller', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {provide: ConfigService, useValue: {}},
        {provide: AuthService, useValue: {}},
        // {provide: UserByIdPipe, useValue: {}},
        {provide: UserService, useValue: {}},
      ]
    })
    .overrideProvider(UserByIdPipe)
    .useValue({})
    .compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
