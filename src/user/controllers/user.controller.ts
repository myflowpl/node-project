import { Controller, Post, Body, InternalServerErrorException, Get, Request, UseGuards, HttpException, HttpStatus, UsePipes, ValidationPipe, Headers, Param, ParseIntPipe } from '@nestjs/common';
import { ApiCreatedResponse, ApiBearerAuth, ApiImplicitParam } from '@nestjs/swagger';
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from '../dto';
import { User } from '../decorators/user.decorator';
import { Roles } from '../decorators/roles.decorator';
import { UserRole, UserModel } from '../models';
import { AuthGuard } from '../guards/auth.guard';
import { AuthService, UserService } from '../services';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';
import { Observable } from 'rxjs';
import { ClientProxy, Transport, Client } from '@nestjs/microservices';

@Controller('user')
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) { }



  @Client({ transport: Transport.TCP, options: { port: 3001 } })
  client: ClientProxy;

  @Get('sum')
  sum(): Observable<number> {
    const pattern = { cmd: 'sum' };
    const payload = [1, 2, 3];
    return this.client.send<number>(pattern, payload);
  }



  
  @Post('register')
  @ApiCreatedResponse({ type: UserRegisterResponseDto })
  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {

    let user;
    try {
      user = await this.userService.create(data);
      return {
        user,
      };
    } catch (error) {
      // TODO handle errors
      throw new InternalServerErrorException('Error', error);
    }
  }

  @Get()
  // @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  getUser(@User() user) {
    return user;
  }

  @Post('login')
  @UsePipes(new ValidationPipe({ transform: true }))
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }

    return {
      token: await this.authService.tokenSign({ user }),
      user,
    };
  }

  @Get(':id')
  @ApiImplicitParam({ name: 'id', type: String })
  getUserById(@Param('id', ParseIntPipe, UserByIdPipe) user: UserModel) {
    return user;
  }
}
